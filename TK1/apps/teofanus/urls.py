from django.urls import path

from .views import login, register, register_account, login_account, logout_account

app_name = "teofanus"

urlpatterns = [
    path('login/', login, name="login"),
    path('register/', register, name="register"),
    path('register_account/', register_account, name="register_account"),
    path('login_account/', login_account, name="login_account"),
    path('logout_account', logout_account, name="logout_account"),
]
