from django.apps import AppConfig


class TeofanusConfig(AppConfig):
    name = 'teofanus'
