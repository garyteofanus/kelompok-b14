from django.test import TestCase
from django.urls import reverse
from django.test import Client
from TK1.apps.main.models import Tips, News
from django.core.files.uploadedfile import SimpleUploadedFile

class UniTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up data for the whole TestCase
        cls.home = Client().get(reverse('main:home'))
        cls.tips = Client().get(reverse('main:tips'))
        cls.news = Client().get(reverse('main:news'))
        
    def test_home_path_page_exist(self):
        self.assertEqual(self.home.status_code, 200)

    def test_tips_path_page_exist(self):
        self.assertEqual(self.tips.status_code, 200)

    def test_news_path_page_exist(self):
        self.assertEqual(self.news.status_code, 200)

    # Test if the correct template is used

    def test_home_template_is_used(self):
        self.assertTemplateUsed(self.home, 'main/home.html')

    def test_tips_template_is_used(self):
        self.assertTemplateUsed(self.tips, 'main/tips.html')
    
    def test_news_template_is_used(self):
        self.assertTemplateUsed(self.news, 'main/news.html')

    # Test adding tips

    def test_add_tips(self):
        # Test for adding a successfull tips
        image = open('TK1/static/images/default.png', 'rb')
        data = {
            'title': 'Title Test 1',
            'description': 'Description Test 1',
            'image': SimpleUploadedFile(image.name, image.read())
        }
        response = self.client.post(reverse('main:add_tips'), data, follow = True)
        self.assertEqual(Tips.objects.all().count(), 1)

        # Test correct message
        messages = list(response.context['messages'])
        self.assertEqual(str(
            messages[0]), 'Tips added successfully!')

        # Test if redirections works
        self.assertRedirects(response, reverse('main:home'))

        # Test for adding a wrong tips
        image = open('TK1/static/images/logo.svg', 'rb')
        data = {
            'title': 'Title Test 1',
            'description': 'Description Test 1',
            'image': SimpleUploadedFile(image.name, image.read())
        }
        response = self.client.post(reverse('main:add_tips'), data, follow = True)
        self.assertEqual(Tips.objects.all().count(), 1) # Compared to one because of previous test that adds a tips

        # Test correct message
        messages = list(response.context['messages'])
        self.assertEqual(str(
            messages[0]), 'The uploaded file must be an image!')

        # Test if redirections works
        self.assertRedirects(response, reverse('main:tips'))

        # Test if request is GET
        response = self.client.get(reverse('main:add_tips'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/home.html')

    # Test removing tips

    def test_remove_tips(self):
        image = open('TK1/static/images/default.png', 'rb')
        tips = Tips.objects.create(
            title = 'Tips Test',
            description = 'Tips Description Test',
            image = SimpleUploadedFile(image.name, image.read())
        )
        response = self.client.get(reverse('main:remove_tips', args=[tips.id]), follow = True)
        self.assertEqual(News.objects.all().count(), 0)

        # Test correct message
        messages = list(response.context['messages'])
        self.assertEqual(str(
            messages[0]), 'Tips has been deleted')

        # Test if redirection works
        self.assertRedirects(response, reverse('main:home'))
    
    # Test adding news
    
    def test_add_news(self):
        # Test adding a correct news
        response = self.client.post(reverse('main:add_news'), 
            {
                'title': 'Satgas Ungkap Sebab Testing Covid-19 Indonesia di Bawah WHO', 
                'link': 'https://www.cnnindonesia.com/nasional/20201118151530-20-571399/satgas-ungkap-sebab-testing-covid-19-indonesia-di-bawah-who', 
                'image_headline': 'https://akcdn.detik.net.id/visual/2020/10/03/swab-test-drive-thru-8_169.jpeg?w=650'
            }, follow = True)
        self.assertEqual(News.objects.all().count(), 1)

        # Test correct message
        messages = list(response.context['messages'])
        self.assertEqual(str(
            messages[0]), 'News added successfully!')

        # Test if redirections works
        self.assertRedirects(response, reverse('main:home'))

        # Test when request is GET
        response = self.client.get(reverse('main:add_news'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/home.html')

    # Test removing news

    def test_remove_news(self):
        news = News.objects.create(
            title = 'Satgas Ungkap Sebab Testing Covid-19 Indonesia di Bawah WHO',
            link = 'https://www.cnnindonesia.com/nasional/20201118151530-20-571399/satgas-ungkap-sebab-testing-covid-19-indonesia-di-bawah-who',
            image_headline = 'https://akcdn.detik.net.id/visual/2020/10/03/swab-test-drive-thru-8_169.jpeg?w=650'
        )
        response = self.client.get(reverse('main:remove_news', args=[news.id]), follow = True)
        self.assertEqual(News.objects.all().count(), 0)

        # Test correct message
        messages = list(response.context['messages'])
        self.assertEqual(str(
            messages[0]), 'News has been deleted')

        # Test if redirection works
        self.assertRedirects(response, reverse('main:home'))
