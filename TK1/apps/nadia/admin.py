from django.contrib import admin
from .models import KotaModel
from .models import TempatModel

# Register your models here.

admin.site.register(KotaModel)
admin.site.register(TempatModel)
