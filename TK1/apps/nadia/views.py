from django.shortcuts import render, redirect 

from .models import KotaModel, TempatModel

# from .filters import KotaFilter

# Create your views here.

def lokasiTest(request):
    tempats = TempatModel.objects.all()
    if request.method == 'POST':
        query = request.POST['lokasi']
        tempats = tempats.filter(alamat__icontains=query)
        # context['query'] = query
        # context['allitems'] = allitems

    # myFilter = KotaFilter(request.GET, queryset = tempats)
    # tempats = myFilter.qs
    context = {
        'tempats': tempats,
        # 'myFilter': myFilter,
    }
    return render(request, "lokasiTest.html", context)
